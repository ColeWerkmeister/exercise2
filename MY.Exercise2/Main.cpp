#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>
using namespace std;


enum Suit
{
	CLOVER = 1, DIAMOND = 2, HEART = 3, SPADE = 4
};

enum Rank
{
	TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7, EIGHT = 8, NINE = 9, TEN = 10, JACK = 11, QUEEN = 12, KING = 13, ACE = 14
};

struct Cards
{
	Suit suit;
	Rank rank;
};

int generator1()
{
	int ranSuit = rand() % 4 + 1;
	return ranSuit;
}

int generator2()
{
	int ranRank = rand() % 14 + 1;
	return ranRank;
}

Cards determinCard()
{
	Cards card;
	int ranSuit = generator1();
	int ranRank = generator2();

	switch (ranSuit) {
	case 1:
		card.suit = CLOVER;
		break;
	case 2:
		card.suit = DIAMOND;
		break;
	case 3:
		card.suit = HEART;
		break;
	case 4:
		card.suit = SPADE;
		break;
	default:
		card.suit = CLOVER;
	}

	switch (ranRank) {
	case 2:
		card.rank = TWO;
		break;
	case 3:
		card.rank = THREE;
		break;
	case 4:
		card.rank = FOUR;
		break;
	case 5:
		card.rank = FIVE;
		break;
	case 6:
		card.rank = SIX;
		break;
	case 7:
		card.rank = SEVEN;
		break;
	case 8:
		card.rank = EIGHT;
		break;
	case 9:
		card.rank = NINE;
		break;
	case 10:
		card.rank = TEN;
		break;
	case 11:
		card.rank = JACK;
		break;
	case 12:
		card.rank = QUEEN;
		break;
	case 13:
		card.rank = KING;
		break;
	case 14:
		card.rank = ACE;
		break;
	default:
		card.rank = TWO;
	}
	return card;
	_getch();
}

void PrintCard(Cards card)
{
	switch(card.rank) {
	case 2:
		cout << "The Two of ";
		break;
	case 3:
		cout << "The Three of ";
		break;
	case 4:
		cout << "The Four of ";
		break;
	case 5:
		cout << "The Five of ";
		break;
	case 6:
		cout << "The Six of ";
		break;
	case 7:
		cout << "The Seven of ";
		break;
	case 8:
		cout << "The Eight of ";
		break;
	case 9:
		cout << "The Nine of ";
		break;
	case 10:
		cout << "The Ten of ";
		break;
	case 11:
		cout << "The Jack of ";
		break;
	case 12:
		cout << "The Queen of ";
		break;
	case 13:
		cout << "The King of ";
		break;
	case 14:
		cout << "The Ace of ";
		break;
	}

	switch (card.suit) {
	case 1:
		cout << "Clovers";
		break;
	case 2:
		cout << "Diamonds";
		break;
	case 3:
		cout << "Hearts";
		break;
	case 4:
		cout << "Spades";
	}
}

int main()
{
	srand(time(NULL));
	int answer;
	cout << "Print Card - Enter 1" << endl;
	cout << "High Card - Enter 2" << endl;
	cin >> answer;

	if (answer == 1)
	{
		Cards card = determinCard();
		PrintCard(card);
		_getch();
	}
	else if (answer == 2)
	{
		Cards card1 = determinCard();
		Cards card2 = determinCard();
		PrintCard(card1);
		cout << endl;
		PrintCard(card2);
		cout << endl;
		cout << endl;

		if (card1.rank > card2.rank)
		{
			PrintCard(card1);
			cout << " has a higher rank";
		}
		else if (card1.rank < card2.rank)
		{
			PrintCard(card2);
			cout << " has a higher rank";
		}
		else
		{
			cout << "Card ranks are equal";
		}

		_getch();
	}
	else
	{
		cout << "Invalid Entry";
	}

	
	return 0;
}
